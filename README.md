# exmedia.sh

Script en Bash para extraer los capítulos de las series descargadas de sus correspondientes directorios y renombrarlos.

## A tener en cuenta:

Los archivos/directorios de las series a extraer/renombrar deben estar __todos__ dentro del mismo directorio, esto es así para prevenir que otros archivos _mkv_ o _avi_ que se encuentren en otros subdirectorios sean desplazados de su lugar.

Es recomendable ubicar el _script_ en el directorio `~/bin` , si no existe puedes crearlo.

### Uso:

Dar permisos de ejecución:

~~~sh
chmod u+x exmedia.sh
~~~

Abrir un terminal en el directorio donde se encuentren los archivos correspondientes a las series y ejecutar el script.

Extraer los archivos de _mkv|avi_ al directorio actual y renombrarlos.

~~~bash
$ exmedia.sh
~~~

Si el número de archivos multimedia coincide con el de directorios los elimina y solicita por pantalla el título de la serie.

~~~
Enter the title of the serie :
~~~

Seguido, muestra una previa de como serán renombrados los archivos y solicita confirmación.

~~~
[The files will be renamed as follows...]
  jessica jones 212.mkv <--[ Jessica Jones 720p 2x12 [www.torrentrapid.com].mkv
  jessica jones 211.mkv <--[ Jessica Jones 720p 2x11 [www.torrentrapid.com].mkv
  jessica jones 210.mkv <--[ Jessica Jones 720p 2x10 [www.torrentrapid.com].mkv
  jessica jones 201.mkv <--[ Jessica Jones 720p 2x01 [www.torrentrapid.com].mkv
  jessica jones 203.mkv <--[ Jessica Jones 720p 2x03 [www.torrentrapid.com].mkv
  jessica jones 207.mkv <--[ Jessica Jones 720p 2x07 [www.torrentrapid.com].mkv
  jessica jones 205.mkv <--[ Jessica Jones 720p 2x05 [www.torrentrapid.com].mkv
  jessica jones 208.mkv <--[ Jessica Jones 720p 2x08 [www.torrentrapid.com].mkv
  jessica jones 202.mkv <--[ Jessica Jones 720p 2x02 [www.torrentrapid.com].mkv
  jessica jones 204.mkv <--[ Jessica Jones 720p 2x04 [www.torrentrapid.com].mkv
  jessica jones 209.mkv <--[ Jessica Jones 720p 2x09 [www.torrentrapid.com].mkv
  jessica jones 213.mkv <--[ Jessica Jones 720p 2x13 [www.torrentrapid.com].mkv
  jessica jones 206.mkv <--[ Jessica Jones 720p 2x06 [www.torrentrapid.com].mkv
Do you want to continue? [y/n]: y

# Si es correcto, escribir `y` y pulsar `Intro`.
~~~

Puede que el número de directorios no coincida con el de archivos multimedia extraidos, en ese caso se muestra el siguiente mensaje y finaliza el script.

```
Error, the number of files (10) and folders (6) is not the same.
```

Esto puede deberse a que algún archivo esté en formato _rar_ o que tengamos más de un archivo en un mismo directorio. 



## Opción `-r`

Esta opción permite renombrar todos los archivos _avi_ o _mkv_ que se encuentren en el directorio actual.

~~~bash
$ exmedia.sh -r
~~~

Esto mostrará el mismo mensaje anterior para poder revisar que  el renombrado sea el correcto.



## Opción `-o`

Es posible que el título de la serie contenga algún número o que la cadena del nombre del archivo tenga otro orden distinto al habitual, en estos casos puede que no se muestren correctamente los números de los capítulos.

Para estos casos indicaremos con la opción `-o` en número de carácteres a desplazar (habitualmente con uno es suficiente).

~~~bash
$ exmedia.sh -o 1 -r
~~~

> La opción `-o #N` debe preceder **siempre** a la opción `-r`.

Comprobar que es correcto e introducir  `y` seguido de `Intro`.

