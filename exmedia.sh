#!/usr/bin/env bash
# Date: 2019.08.10
# Use: ./exmedia.sh [-h] [-x] [-o N][-r] [-Y] [-a] [-s|S]

readonly NONE='\033[00m'
readonly RED='\033[01;31m'
readonly BOLDWHITE='\033[01;37m'
readonly IFS=$'\n'

MEDIAFILES=($(find . -maxdepth 2 \( -iname '*\.mkv' -o -iname '*\.avi' \)))
NUMDIRS=$(find ./* -type d | wc -l)
NUMFILES="${#MEDIAFILES[@]}"
TITLE=""
SEASON=""
EXTRACT=false
YESNO="n"
MAKEDIRS=false  # Si true: ../season N/

_help_message() {
    cat <<-EOF
Usage: ${0##*/} [-h] [-x] [-o N][-r] [-Y] [-a] [-s|S]
Whitout options, extract and rename all media files in the directory.
-h    Print this help and exit.
-x    Only extract media files from folders.
-o N  Add N number to offset (before -r option).
-r    Only rename files.
-Y    Doesn't ask for confirmation to rename files.
-a    Auto extract data and rename files (take name from parent dir).
-s    Make ../SEASON N/ directories only.
-S    Make ../TITLE/SEASON N/ directories.
EOF
}

# Comprueba si dos archivos tienen el mismo nombre antes de extraerlos (evita sobreescritura)
_check_duplicates() {
    printf '%s\n' "${MEDIAFILES[@]##*/}" | sort | uniq -d > /tmp/duplicate
    repeated="$(wc -l < /tmp/duplicate)"

    if [[ "$repeated" != 0 ]]; then
        echo "Error, ${repeated} files have the same name:"
        echo -e "${RED}$(</tmp/duplicate)${NONE}"
        echo "Rename them and try again."
        exit 1
    fi >&2
}

# Extrae los capitulos. Si coincide el num. de archivos y directorios, borra dirs.
_extract_media() {
    _check_duplicates
    for files in "${MEDIAFILES[@]}"; do
        mv "$files" .
    done

    if [[ "$NUMFILES" -eq "$NUMDIRS" ]]; then
        echo "$NUMFILES files extracted correctly."
        rm -r ./*/ && echo "Deleted all directories."
    else
        local nf="${BOLDWHITE}${NUMFILES}${NONE}"
        local nd="${BOLDWHITE}${NUMDIRS}${NONE}"
        echo -e "Caution, the number of files ($nf) and folders ($nd) is not the same." >&2
    fi
}

# Comprueba si el valor de la opcion [-o] es numerica
_add_offset() {
    if [[ $OPTARG =~ [0-9] ]]; then
        addoffset=$OPTARG
    else
        _help_message
        exit 1
    fi
}

_rename_media() {
    local yesno

    read -r -p "Enter the title: " SERIENAME
    EXT="${MEDIAFILES[0]##*.}"
    # Extrae el offset para obtener el num. de capitulo (si el cap. tiene Num. desplaza N veces).
    OFFSET=$(echo -n "${SERIENAME//[a-Z. -]/}" | wc -m)
    OFFSET=$((OFFSET+addoffset))

    # Vista previa del renombrado de los archivos.
    echo "[The files will be renamed as follows...]"

    for media in "${MEDIAFILES[@]##*/}"; do
        # Num. capitulo: Elimina 720, letras, espacios en blanco y signos de puntuacion.
        # El num. del capitulo debe ser contiguo al titulo de la serie.
        # Ej. serie8.cap101.ms11=810111 ([8]offset[101]num.cap[11]resto)
        BRUTENUM=$(sed -e "s/720p//;s/1080p//;s/[[:alpha:]]//g;s/ //g;s/[[:punct:]]//g" \
        <<<"${media##*/}")
        echo "  $SERIENAME ${BRUTENUM:${OFFSET}:3}.${EXT} <--[ $media"
    done

    # No se permite la opción -Y en este modo debido al alto riesgo de error
    read -r -p "Do you want to continue? [y/n]: " yesno

    if [[ "$yesno" == [Yy] ]]; then
        for media in "${MEDIAFILES[@]##*/}"; do
            BRUTENUM=$(sed -e "s/720p//;s/1080p//;s/[[:alpha:]]//g;s/ //g;s/[[:punct:]]//g" \
            <<<"${media##*/}")
            mv "$media" "$SERIENAME ${BRUTENUM:${OFFSET}:3}.${EXT}" 2> /dev/null
        done
    elif [[ "$yesno" == [Nn] ]]; then
        echo "Operation has been canceled." >&2
        exit 2
    else
        echo "Invalid option, insert [y]es or [n]o." >&2
        exit 3
    fi

    echo "Editing metadata..."
    _edit_metadata
}

_auto_rename_media() {
    # Si se extraen los archivos de los directorios actualiza la ruta de estos
    [ "$EXTRACT" == true ] && {
        local item
        local status=0
        MEDIAFILES=("${MEDIAFILES[@]##*/}")
        # files=(*)
        # # Comprueba si los archivos se encuentran en el directorio actual
        # [[ ! ${MEDIAFILES[@]} == ${files[@]} ]]  && \
        #     echo "Error, no media files found in directory." >&2 && exit 1
        for item in "${MEDIAFILES[@]}"; do
            stat "$item" /dev/null 2>&1
            ((status + $?))
        done
        [ "$status" -gt 0 ] && { 
            echo "No media files found in directory." >&2
            exit; }
    }

    count_season=0
    count_cap=0
    # Comprueba si existen cadenas no validas para la obtención de datos
    for media in "${MEDIAFILES[@]}"; do
        [[ ! "$media" =~ Temporada ]] && ((count_season++))
        [[ ! "$media" =~ \[Cap\. ]] && ((count_cap++))
    done

    if (( count_season + count_cap > 0 )); then
        {
            echo "[CANCELED]"
            echo "Some files contain possibly incompatible strings."
            echo "For more security, run exmedia.sh -r"
        } >&2
        exit 2
    fi 

    # Toma datos en strings tipo: Título - Temporada 1 [HDTV 720p][Cap.101][AC3... 
    local episode
    local ext

    TITLE="${MEDIAFILES[0]%% -*}"
    TITLE="${TITLE##*/}"
    SEASON="${MEDIAFILES[0]%% [*}"
    SEASON="${SEASON##* }"

    printf "%s%b:    %b\n" "Title" "${BOLDWHITE}" "${TITLE}${NONE}"
    printf "%s%b:   %b\n" "Season" "${BOLDWHITE}" "${SEASON}${NONE}"
    printf "%s%b: %b\n\n" "Episodes" "${BOLDWHITE}" "${NUMFILES}${NONE}"

    # Si se habilita la opción -Y, no pide permiso para continuar
    [ "$YESNO" == n ] && read -r -p "Do you want to continue? [y/n]: " YESNO

    if [[ "$YESNO" == [Yy] ]]; then
        echo "Renaming files..."

        for media in "${MEDIAFILES[@]##*/}"; do
            episode="${media##*Cap.}"
            episode="${episode%%]*}"
            ext="${media##*.}"

            # Mensaje: título original -> renombrado
            printf "%s:\t%s\n" "From" "${media}"
            printf "%b%s:\t%b\n" \
                "$BOLDWHITE" "To" "${TITLE,,} ${episode}.${ext} $NONE"

            # Renombra los archivos o finaliza si encuentra algún error
            mv "$media" "${TITLE,,} ${episode}.${ext}" || \
                { echo "Error during file renaming" >&2 && exit 1; }
        done

        echo "All files has been renamed."
    elif [[ "$YESNO" == [Nn] ]]; then
        echo "Operation has been canceled." >&2
        exit
    else
        echo "Invalid option, insert [y]es or [n]o." >&2
        exit 3
    fi

    echo "Editing metadata..."
    _edit_metadata
}

_make_season_structure() (
    # Ejecuta la función en un subshell
    # nullglob provoca problemas con find en la expansión de comodines
    shopt -s nullglob
    # Nueva ruta para la temporada completa
    local path="${TITLE:-title}/season ${SEASON:-0}"
    local old_pwd="$PWD"

    # Con la opción -s, crea sólo el directorio season N/
    [ "$MAKEDIRS" == true ] && path="season ${SEASON:-0}"

    echo "Creating new directory structure for season..."
    mkdir -p ../"${path}"
    # Mueve los archivos, pide confirmación si ya existen en destino
    echo "Moving media files to ${path}..."
    mv -i ./*.{mkv,avi} ../"${path}"

    echo "Changing directory to ${path}..."
    cd ../"${path}" || { echo "Can't change to $path" >&2;
        exit 1; }

    local files=(./*)
    # Elimina el directorio original si se han copiado los archivos
    if [ "${#files[@]}" -eq "$NUMFILES" ]; then
        echo "Delete directory: $old_pwd"
        read -r -p "[y/N]: "
        while true; do
            case "$REPLY" in
                [yY])
                    echo "Removing old directory..."
                    rm -r "$old_pwd"
                    echo "Finished."
                    break
                    ;;
                [nN])
                    echo "Canceled." >&2
                    return 1
                    ;;
                \*) 
                    echo "-${REPLY}; invalid option."
            esac
        done

    else
        echo "The old directory has not been deleted, \
              number of files is not the same" >&2
    fi
)

_edit_metadata() {
    [[ $(which mkvtagedit.sh) ]] && local mkvedit=true
    [[ $(which avitagedit.sh) ]] && local aviedit=true

    if [[ "$mkvedit" && "$aviedit" ]]; then
        case "${MEDIAFILES[0]}" in
            *.mkv)
                mkvtagedit.sh ;;
            *.avi)
                avitagedit.sh ;;
            *)
                echo "Invalid filetype to edit metadata." >&2
                exit 3
            ;;
        esac
    fi
}

### MAIN ###

# Sin opciones extrae y renombra.
if [[ $# == 0 ]]; then
    _extract_media
    _rename_media
    exit 0
fi

while getopts ":hYxo:rasS" option; do
    case $option in
        h)  _help_message ;;
        x)  _extract_media
            EXTRACT=true ;;
        o)  _add_offset ;;
        r)  _rename_media ;;
        Y)  YESNO=y ;;
        a)  _auto_rename_media ;;
        s)  MAKEDIRS=true
            _make_season_structure ;;
        S)  _make_season_structure ;;
        \?) echo -e "Error: ${RED}-${option}${NONE} is not a valid argument." >&2
            _help_message
            exit 3 ;;
        :)  echo "Option -${OPTARG} require an argument." >&2
            exit 3 ;;
    esac
done

exit 0
